package pl.koobe24.minimalisticauditor;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.koobe24.minimalisticauditor.entity.StarsCoinBundle;
import pl.koobe24.minimalisticauditor.entity.Tournament;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by tomas on 14.11.2016.
 */
//po refaktoryzacji 1
public class FileParser {
    private File input;
    private Document document;
    private List<Tournament> tournamentList;
    private List<StarsCoinBundle> bundleList;

    private final String SPIN_AND_GO_NAME_PATTERN = ".+\\$([0]?.?[0-9]+).+SpinGo.+";
    private final String SPIN_AND_GO_ALTERNATE_NAME_PATTERN = ".+\\$([0]?.?[0-9]+).+Spin \\& Go";
    private final String STARS_COIN_BUNDLE = "StarsCoin Bundle";

    private final int ACTION_INDEX = 1;
    private final int GAME_INDEX = 3;
    private final int AMOUNT_INDEX = 5;
    private final int STARS_COIN_AMOUNT_INDEX = 7;

    public FileParser(File input) {
        this.input = input;
        tournamentList = new ArrayList<>();
        bundleList = new ArrayList<>();
    }

    public void parseFile() throws IOException {
        document = Jsoup.parse(input, "UTF-8");
        Elements rows = document.getElementsByTag("tr");
        ListIterator<Element> it = rows.listIterator(2);
        Element e;
        while (it.hasNext()) {
            e = it.next();
            if (isStarsCoinBundle(e.child(1).text())) {
                addStarsCoinBundle(e);
            } else if (isSpinAndGo(e.child(3).text())) {
                addTournament(e);
            }
        }
    }

    private boolean isStarsCoinBundle(String action) {
        return action.equals(STARS_COIN_BUNDLE);
    }

    private void addStarsCoinBundle(Element e) {
        try {
            String action = e.child(ACTION_INDEX).text();
            String coinsAmount = e.child(STARS_COIN_AMOUNT_INDEX).text();
            StarsCoinBundle coinBundle = new StarsCoinBundle(action, coinsAmount);
            bundleList.add(coinBundle);
        } catch (ParseException exception) {
            System.err.println("Nieudane parsowanie ");
        }
    }

    private void addTournament(Element e) {
        try {
            String action = e.child(ACTION_INDEX).text();
            String game = e.child(GAME_INDEX).text();
            String amount = e.child(AMOUNT_INDEX).text();
            Tournament tournament = new Tournament(action, game, amount);
            tournamentList.add(tournament);
        } catch (ParseException exception) {
            System.err.println("Nieudane parsowanie amount dla turnieju.");
        }
    }

    private boolean isSpinAndGo(String name) {
        return name.matches(SPIN_AND_GO_NAME_PATTERN) ||
                name.matches(SPIN_AND_GO_ALTERNATE_NAME_PATTERN);
    }

    public List<Tournament> getTournamentList() {
        return tournamentList;
    }

    public List<StarsCoinBundle> getBundleList() {
        return bundleList;
    }
}
