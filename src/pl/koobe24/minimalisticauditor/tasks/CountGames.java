package pl.koobe24.minimalisticauditor.tasks;

import pl.koobe24.minimalisticauditor.entity.Tournament;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 14.11.2016.
 */
//tu refaktoryzacja raczej niepotrzebna
public class CountGames extends RecursiveTask<Map<Double, Integer>> {

    private List<Tournament> tournamentList;
    private int start, end;
    private final int seqThreshold = 1000;

    public CountGames(List<Tournament> tournamentList, int start, int end) {
        this.tournamentList = tournamentList;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Map<Double, Integer> compute() {
        Map<Double, Integer> map = new TreeMap<>();
        if ((end - start) < seqThreshold) {
            Integer count;
            Tournament t;
            for (int i = start; i < end; i++) {
                t = tournamentList.get(i);
                if (t.getAmount() < 0) {
                    count = map.containsKey(t.getAmount()) ? map.get(t.getAmount()) : 0;
                    map.put(t.getAmount(), count + 1);
                }
            }
        } else {
            int middle = (start + end) / 2;
            CountGames task1 = new CountGames(tournamentList, start, middle);
            CountGames task2 = new CountGames(tournamentList, middle, end);
            task1.fork();
            task2.fork();

            Map<Double, Integer> m1 = task1.join();
            Map<Double, Integer> m2 = task2.join();
            m1.forEach((k, v) -> m2.merge(k, v, (v1, v2) -> v1 + v2));
            map = m2;
        }

        return map;
    }
}
