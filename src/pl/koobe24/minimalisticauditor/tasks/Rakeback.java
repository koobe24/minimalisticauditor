package pl.koobe24.minimalisticauditor.tasks;

import pl.koobe24.minimalisticauditor.entity.StarsCoinBundle;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 14.11.2016.
 */
//tu feraktoryzacja raczej niepotrzebna
public class Rakeback extends RecursiveTask<Double> {
    private List<StarsCoinBundle> starsCoinBundleList;
    private int start, end;
    private final int seqThreshold = 1000;

    public Rakeback(List<StarsCoinBundle> starsCoinBundleList, int start, int end) {
        this.starsCoinBundleList = starsCoinBundleList;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        Double sum = 0.0;
        if ((end - start) < seqThreshold) {
            for (int i = start; i < end; i++) {
                sum += starsCoinBundleList.get(i).getAmount();
            }
        } else {
            int middle = (start + end) / 2;
            Rakeback subTaskA = new Rakeback(starsCoinBundleList, start, middle);
            Rakeback subTaskB = new Rakeback(starsCoinBundleList, middle, end);
            subTaskA.fork();
            subTaskB.fork();

            sum = subTaskA.join() + subTaskB.join();
        }
        return sum;
    }
}
