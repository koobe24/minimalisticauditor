package pl.koobe24.minimalisticauditor.tasks;

import pl.koobe24.minimalisticauditor.entity.Tournament;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 14.11.2016.
 */
//tu refaktoryzacja raczej niepotrzebna
public class SpinGoProfit extends RecursiveTask<Double> {
    private List<Tournament> tournamentList;
    private int start, end;
    private final int seqThreshold = 1000;

    public SpinGoProfit(List<Tournament> tournamentList, int start, int end) {
        this.tournamentList = tournamentList;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        Double sum = 0.0;
        if ((end - start) < seqThreshold) {
            for (int i = start; i < end; i++) {
                sum += tournamentList.get(i).getAmount();
            }
        } else {
            int middle = (start + end) / 2;
            SpinGoProfit subTaskA = new SpinGoProfit(tournamentList, start, middle);
            SpinGoProfit subTaskB = new SpinGoProfit(tournamentList, middle, end);
            subTaskA.fork();
            subTaskB.fork();

            sum = subTaskA.join() + subTaskB.join();
        }
        return sum;
    }
}
