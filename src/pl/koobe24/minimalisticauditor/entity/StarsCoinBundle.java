package pl.koobe24.minimalisticauditor.entity;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by tomas on 14.11.2016.
 */
//po refaktoryzacji 1
public class StarsCoinBundle {
    private String action;
    private Double amount;

    public StarsCoinBundle(String action, String amount) throws ParseException {
        this.action = action;
        NumberFormat format=NumberFormat.getNumberInstance(Locale.US);
        this.amount = format.parse(amount).doubleValue();
    }

    public Double getAmount() {
        return amount;
    }
}
