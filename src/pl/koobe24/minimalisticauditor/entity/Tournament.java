package pl.koobe24.minimalisticauditor.entity;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by tomas on 14.11.2016.
 */
//po refaktoryzacji 1
public class Tournament {
    private String action;
    private String game;
    private Double amount;

    public Tournament(String action, String game, String amount) throws ParseException {
        this.action = action;
        this.game = game;
        this.amount = parseNegativeDouble(amount);
    }

    private Double parseNegativeDouble(String amount) throws ParseException {
        amount = replaceParenthesis(amount);
        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
        return format.parse(amount).doubleValue();
    }

    private String replaceParenthesis(String amount) {
        if (amount.contains("(") || amount.contains(")")) {
            amount = amount.replace("(", "-");
            amount = amount.replace(")", "");
        }
        return amount;
    }

    public Double getAmount() {
        return amount;
    }
}
