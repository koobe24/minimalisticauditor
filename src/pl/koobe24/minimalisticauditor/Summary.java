package pl.koobe24.minimalisticauditor;

import pl.koobe24.minimalisticauditor.entity.StarsCoinBundle;
import pl.koobe24.minimalisticauditor.entity.Tournament;
import pl.koobe24.minimalisticauditor.tasks.CountGames;
import pl.koobe24.minimalisticauditor.tasks.Rakeback;
import pl.koobe24.minimalisticauditor.tasks.SpinGoProfit;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by tomas on 14.11.2016.
 */
//po pierwszej refaktoryzacji
public class Summary {

    private List<Tournament> tournamentList;
    private List<StarsCoinBundle> starsCoinBundleList;
    private Double profit;
    private Double rakeback;
    private Map<Double, Integer> gamesMap;
    private Integer howMany;

    public Summary(List<Tournament> tournamentList, List<StarsCoinBundle> starsCoinBundleList) {
        this.tournamentList = tournamentList;
        this.starsCoinBundleList = starsCoinBundleList;
        howMany = 0;
    }

    public void countTotals() {
        ForkJoinPool fjp = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        countProfit(fjp);
        countRakeback(fjp);
        countGames(fjp);
    }

    private void countProfit(ForkJoinPool fjp) {
        SpinGoProfit task = new SpinGoProfit(tournamentList, 0, tournamentList.size());
        profit = fjp.invoke(task);
    }

    private void countRakeback(ForkJoinPool fjp) {
        Rakeback task2 = new Rakeback(starsCoinBundleList, 0, starsCoinBundleList.size());
        rakeback = fjp.invoke(task2) / 100;
    }

    private void countGames(ForkJoinPool fjp) {
        CountGames task3 = new CountGames(tournamentList, 0, tournamentList.size());
        gamesMap = fjp.invoke(task3);
        gamesMap.forEach((k, v) -> howMany += v);
    }

    public Integer getHowMany() {
        return howMany;
    }

    public Double getProfit() {
        return profit;
    }

    public Double getRakeback() {
        return rakeback;
    }

    public Map<Double, Integer> getGamesMap() {
        return gamesMap;
    }
}
