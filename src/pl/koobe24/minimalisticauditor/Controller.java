package pl.koobe24.minimalisticauditor;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

public class Controller implements Initializable {

    @FXML
    private Button openButton;

    @FXML
    private TextField filePath;

    private FileChooser fileChooser;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("html files (*.html)", "*.html");
        fileChooser.getExtensionFilters().add(filter);
        openButton.setOnAction(event -> {
            Preferences prefs = Preferences.userNodeForPackage(pl.koobe24.minimalisticauditor.Main.class);
            String defaultValue = prefs.get("file_chooser_path", "");
            if (!defaultValue.equals("")) {
                fileChooser.setInitialDirectory(new File(defaultValue));
            }
            File file = fileChooser.showOpenDialog(openButton.getScene().getWindow());
            if (file != null) {
                prefs.put("file_chooser_path", file.getParentFile().getAbsolutePath());
                filePath.setText(file.getAbsolutePath());
                FileParser parser = new FileParser(file);
                try {
                    parser.parseFile();
                    Summary sum = new Summary(parser.getTournamentList(), parser.getBundleList());
                    sum.countTotals();
                    saveToTxt(sum);
                } catch (IOException e) {
                    System.err.println("Blad odczytywania pliku");
                }
                try{
                    ProcessBuilder pb = new ProcessBuilder("notepad.exe", "audit.txt");
                    pb.start();
                } catch(IOException e){
                    System.out.println("Nie udalo sie otworzyc notatnika.");
                }
            }
        });
    }

    //todo zastanowic sie moze ta funkcje przeniesc do oddzielnej klasy?
    //podzielic kolejne linie na oddzielne metody (ale to moze byc bez sensu)
    private void saveToTxt(Summary sum) {
        try (FileWriter fileWriter = new FileWriter(new File("audit.txt"))) {
            fileWriter.write("Total number of Spin&Gos played: " + sum.getHowMany() + System.lineSeparator());
            fileWriter.write("Count by stakes:" + System.lineSeparator());
            for (Map.Entry<Double, Integer> e : sum.getGamesMap().entrySet()) {
                String pom = e.getKey().toString().replace("-", "$") + ": " + e.getValue().toString() + System.lineSeparator();
                fileWriter.write(pom);
            }
            fileWriter.write("Total profit from Spin&Gos: $" + sum.getProfit() + System.lineSeparator());
            Double rakeback = sum.getRakeback();
            fileWriter.write("Total rakeback from StarsCoin: $" + rakeback + System.lineSeparator());
            fileWriter.write("Total profit+RB to split: $" + (sum.getProfit() + rakeback) + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
